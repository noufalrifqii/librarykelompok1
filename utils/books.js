const fs = require("fs");

const loadBook = () => {
  const fileBuffer = fs.readFileSync("data/books.json", "utf-8");
  const books = JSON.parse(fileBuffer);
  return books;
};

module.exports = { loadBook };
