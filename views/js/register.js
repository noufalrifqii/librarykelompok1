function validateForm() {
  var fname = document.getElementById("fname").value;
  var lname = document.getElementById("lname").value;
  var email = document.getElementById("emailaddress").value;
  var pass = document.getElementById("pass").value;
  var gender = document.querySelector('input[type="radio"]:checked').value;
  var birthday = document.getElementById("birthday").value;
  var picture = document.getElementById("picture").value;
  var check = document.getElementById("check").value;
  if (pass.match(/[A-Z]/) && pass.length >= 6) {
    document.write(
      "First Name : " +
        fname +
        "<br>" +
        "Last Name : " +
        lname +
        "<br>" +
        "Email Address : " +
        email +
        "<br>" +
        "Password : " +
        pass +
        "<br>" +
        "Gender : " +
        gender +
        "<br>" +
        "Birthday : " +
        birthday +
        "<br>" +
        "Picture : " +
        picture +
        "<br>" +
        "Check : " +
        check +
        "<br>"
    );
    var btn = document.createElement("button");
    btn.innerHTML = "Back";
    document.body.append(btn);
    btn.style.marginTop = "1rem";
    btn.addEventListener("click", function () {
      location.reload();
    });
  } else {
    document.getElementById("error").innerHTML = "Password must contain uppercase and consist of at least 6 letters";
    document.getElementById("error").style.color = "red";
    return false;
  }
}
