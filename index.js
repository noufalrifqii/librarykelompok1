const express = require("express");
const { handle } = require("express/lib/application");
const app = express();
const PORT = process.env.PORT || 8000;
const expressLayouts = require("express-ejs-layouts");
const { loadBook } = require("./utils/books");

app.set("view engine", "ejs");
app.use(express.static("views"));
app.use(expressLayouts);

app.get("/", (req, res) => {
  const books = loadBook();
  res.status(200).render("pages/index", {
    title: "Home Page",
    layout: "layout/main-layout",
    books,
  });
});

app.get("/register", (req, res) => {
  res.status(200).render("pages/register", {
    title: "Register Page",
    layout: "layout/main-layout",
  });
});

app.get("/buy", (req, res) => {
  res.status(200).render("pages/buy", {
    title: "Buy Page",
    layout: "layout/main-layout",
  });
});

app.use((req, res) => {
  res.status(404).render("pages/404");
});

app.listen(PORT, () => {
  console.log(`Express nyala di http://localhost:${PORT}`);
});
